<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #F26419;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 16px;
  padding: 10px;
  width: 175px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -15px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

.alert-danger{
  display:none;
}

</style>
<x-app-layout>
<title>查看其他使用者評論</title>    
<div id="header-wrapper">
    
    @if($country == 'jp')
        <div class="container p-3 my-3 text-white" style="background-color:#2F4858;width:500px;text-align:center;" >{{$hotel[0]->hotelname}} 的所有評論</div>
          @foreach ($comment as $com)
          <div class="container-sm p-3 border-4 border-dark border-top-0 border-bottom-0 border-right-0" style ="box-shadow: 4px 4px 8px 0 grey;background-color" >
          <div style="font-size:30px;">評論者 : {{$name_arr[$loop->index]}}</div>
          <div style="font-size:30px;">評論內容 : {{$com->content}}</div>
          <div style="font-size:30px;">飯店評分 : {{$com->rating}}</div>
          </div>
          <div class="container-sm p-3"></div>
          @endforeach
        
    @endif
    
    @if($country =='sg')
      <div class="container p-3 my-3 text-white" style="background-color:#2F4858;width:500px;text-align:center;" >{{$hotel[0]->hotelname}} 的所有評論</div>
        @foreach ($comment as $com)
        <div class="container-sm p-3 border-4 border-dark border-top-0 border-bottom-0 border-right-0" style ="box-shadow: 4px 4px 8px 0 grey;background-color" >
          <div style="font-size:30px;">評論者 : {{$user->name}}</div>
          <div style="font-size:30px;">評論內容 : {{$com->content}}</div>
          <div style="font-size:30px;">飯店評分 : {{$com->rating}}</div>
        </div>
        <div class="container-sm p-3"></div>
        @endforeach
    @endif
    <div class="container-sm p-3">
    <form action="{{ url('search/result/'.$country.'/'.$hotel[0]->hotel_id.'/comment') }}" method="POST" id="message-form-new">
        {{ csrf_field() }}
            
        <div style="padding-right: 50px " required="required">評論</div>           
        <textarea class="add_word" name='content' id="textarea" ></textarea>
        <div id='alert' class="alert alert-danger alert-dismissable">評論字數需小於50字</div> 
        
        <div style="padding-right: 50px " required="required">評分</div>
        <div>
            <input type="radio" name="rating" value="1" required>1
            <input type="radio" name="rating" value="2">2
            <input type="radio" name="rating" value="3">3
            <input type="radio" name="rating" value="4">4
            <input type="radio" name="rating" value="5">5   
        </div>   
        <div>
            <!--<input type="submit" value="送出" class="btn btn-primary" >-->
            <button  type="submit" class="btn btn-primary">確定</button>
        </div>  
    </form>
    </div>
    
    <div class="container-sm p-3">
    <a href="/search/result?country={{$country}}&search={{$location}}">
        <button class="button" style="vertical-align:middle"><span>返回查看其他飯店</span></button>  
    </a>
    </div>
</x-app-layout>

<script>
$(document).ready(function(){
  $('#textarea').bind('input propertychange', function() {
    if (this.value.length > 50) {
      $("#alert").show();
      $("#textarea").val($("#textarea").val().substring(0,50));
    }
    else{
      $("#alert").hide();
    }
  });
});

</script>