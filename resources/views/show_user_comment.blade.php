<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #F26419;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 16px;
  padding: 10px;
  width: 170px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

.alert-danger{
  display:none;
}

div.card {
  width: 250px;
  box-shadow: 2px 2px 8px 0 grey;
  text-align: center;
}

div.header {
  background-color: #ee9b00;
  color: white;
  padding: 30px;
  font-size:40px;
}

.container-sm span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.container-sm:hover span {
  padding-bottom: 15px;
  padding-right: 15px;
}

</style>
<title>會員歷史評論</title>
<x-app-layout>
    <div class="container p-3 my-3 text-white" style="background-color:#2F4858">
    
      <a href = '/usercomment/jp'>
          <button class="button" style="vertical-align:middle"><span>查看日本評論</span></button>  
      </a>
      <a href = '/usercomment/sg'>
          <button class="button" style="vertical-align:middle "><span>查看新加坡評論</span></button>  
      </a>
    </div> 
    
    @foreach($hotelnames as $h)
    
    <div class="container-sm p-3 border-4 border-dark border-top-0 border-bottom-0 border-right-0 " style="box-shadow: 4px 4px 8px 0 grey;background-color:">
      <div class="row" >
        <div class="col-6" style="font-size:30px;">{{$h}}</div>
        
      </div>
      <div class="card">
        <div class="header">
          <div>{{$comments[$loop->index]->rating}}分</div>
        </div>
        <div class="container">
          <p>{{$comments[$loop->index]->content}}</p>
        </div>
        <span>
          <div class="container-sm p-3 ">
          <form action="{{ url('/usercomment/'.$country.'/'.$comments[$loop->index]->id.'/delete') }}" method="post">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="{{'#myModal'.$comments[$loop->index]->id}}" >編輯評論</button>
            {!! csrf_field() !!}
            {!! method_field('DELETE') !!}
            <button type="submit" class="btn btn-danger" data-bs-dismiss="modal" onClick="return confirm('確定要刪除嗎？');">刪除評論</button>
          </form>
          </div>
        </span>
      </div>
      
    </div>
    
    <!-- The Modal -->
    <div class="modal" id="{{'myModal'.$comments[$loop->index]->id}}">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class = "modal-header">
            <div style="right:100px;"><button type="button" class="btn-close" data-bs-dismiss="modal" right= "150px"></button></div>
          </div>
          <!-- Modal body -->
          <div class="modal-body" align = "center">
            <div>請輸入新評論</div>
            <form action="{{ url('/usercomment/'.$comments[$loop->index]->id.'/'.$country.'/edit') }}" method="POST">
              {!! csrf_field() !!}  
              <input type="hidden" name = "id" value = "{{$comments[$loop->index]->id}}">
                <textarea class="add_word" name='content' id="textarea{{$comments[$loop->index]->id}}" required>{{$comments[$loop->index]->content}}</textarea>
                <div id='alert{{$comments[$loop->index]->id}}' class="alert alert-danger alert-dismissable">評論字數需小於50字</div>
                <div>
                  <input type="radio" name="rating" value="1" {{ ($comments[$loop->index]->rating==1)? "checked" : "" }} >1</label>
                  <input type="radio" name="rating" value="2" {{ ($comments[$loop->index]->rating==2)? "checked" : "" }} >2</label>
                  <input type="radio" name="rating" value="3" {{ ($comments[$loop->index]->rating==3)? "checked" : "" }} >3</label>
                  <input type="radio" name="rating" value="4" {{ ($comments[$loop->index]->rating==4)? "checked" : "" }} >4</label>
                  <input type="radio" name="rating" value="5" {{ ($comments[$loop->index]->rating==5)? "checked" : "" }} >5</label>
                </div>
                <div style = "padding:10px 15px;">
                  <button  type="submit" class="btn btn-primary" data-bs-dismiss="modal">確定</button>
                  <button  type="button" class="btn btn-danger" data-bs-dismiss="modal">取消</button>
                </div>
                
            </form>
          </div>
          
        </div>
      </div>
    </div>
    <div class="container-sm p-3"></div>
    @endforeach

</x-app-layout>   

<script>
$(document).ready(function(){
  
  @foreach($hotelnames as $h)
  $('#textarea{{$comments[$loop->index]->id}}').bind('input propertychange', function() {
      if (this.value.length > 50) {
        $("#alert{{$comments[$loop->index]->id}}").show();
        $("#textarea{{$comments[$loop->index]->id}}").val($("#textarea{{$comments[$loop->index]->id}}").val().substring(0,50));
      }
      else{
        $("#alert{{$comments[$loop->index]->id}}").hide();
      }
    });
  @endforeach
});


</script>

