<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
</style>
<x-app-layout>
<h1>管理員編輯會員</h1>
<form action= '/admin/userEdit' method="POST"> 
    @csrf
    <input type="hidden" name="id" value="{{$data['id']}}">

    <label for="name">名字</label>
    <input type="text" name="name" value="{{$data['name']}}"><br><br>
    <div style="color:red">{{ $errors->first('name')}}</div>

    <label for="email">電子信箱</label>
    <input type="text" name="email" value="{{$data['email']}}"><br><br>
    <div style="color:red">{{ $errors->first('email')}}</div>

    <td>
        <label for="usertype">使用者類型</label><br>
        <input type="radio" name="usertype" value="0" {{ ($data['usertype'])=='0' ? "checked" : "" }}>會員<br>
        <input type="radio" name="usertype" value="1" {{ ($data['usertype'])=='1' ? "checked" : "" }}>管理員<br>   
    </td>
    <br>
    <div style="color:red">{{ $errors->first('usertype')}}</div>

    <label for="phone">手機</label>
    <input type="text" name="phone" value="{{$data['phone']}}"><br><br>
    <div style="color:red">{{ $errors->first('phone')}}</div>

    <button type='submit' class="button" style="vertical-align:middle">儲存變更</button>
</form>

<a href = "/admin/manage_member">
    <button class="button" style="vertical-align:middle"><span>取消</span></button>  
</a>
</x-app-layout>