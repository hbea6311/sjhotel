<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- 顯示User的評論 -->
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

</style>
<x-app-layout>

<a href = '/admin/comment/jp'>
  <button class="button" style="vertical-align:middle"><span>查看日本評論</span></button>  
</a>
<a href = '/admin/comment/sg'>
  <button class="button" style="vertical-align:middle"><span>查看新加坡評論</span></button>  
</a> 
<hr>

<div class="table-responsive-md container-sm p-3">
  <table class="table table-dark table-hover">
      <tr>
          <td>評論ID</td>
          <td>會員ID</td>
          <td>飯店ID</td>
          <td>內容</td>
          <td>評分</td>
          <td>功能</td>
      </tr>
      @foreach($comments as $comment)
      <tr class = "row_hover">
          <td>{{$comment['id']}}</td>
          <td>{{$comment['user_id']}}</td>
          <td>{{$comment['hotel_id']}}</td>
          <td>{{$comment['content']}}</td>
          <td>{{$comment['rating']}}</td>
          <td>
            <a class='btn btn-danger' href={{'/admin/commentDelete/'.$country.'/'.$comment['id']}}  onClick="return confirm('確定要刪除嗎？');">刪除</a></div>
            <!-- <a class='btn btn-warning' href={{'/admin/commentEdit/'.$country.'/'.$comment['id']}}>編輯</a> -->
          </td>
      </tr>    
      @endforeach
  </table>
</div>
</x-app-layout>