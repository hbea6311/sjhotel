<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

.alert-danger{
  display:none;
}

</style>
<x-app-layout>
<h1>管理員編輯評論</h1>
<div>
  
</div>
<form action={{'/admin/commentEdit/'.$country.'/'.$id}} method="POST">
    @csrf
    <input type="hidden" name="id" value="{{$data['id']}}">
    <input type="hidden" name="user_id" value="{{$data['user_id']}}">
    <input type="hidden" name="hotel_id" value="{{$data['hotel_id']}}">
    <label for="name">內容</label>
    <br>
    <textarea id="w3review" name="content" rows="4" cols="30">{{$data['content']}}</textarea><br>
    <div style="color:red">{{ $errors->first('content')}}</div>
    <br>
    <td>
        <label for="rating">評分</label><br>
        <input type="radio" name="rating" value="1" {{ ($data['rating'])==1 ? "checked" : "" }}>1<br>
        <input type="radio" name="rating" value="2" {{ ($data['rating'])==2 ? "checked" : "" }}>2<br>
        <input type="radio" name="rating" value="3" {{ ($data['rating'])==3 ? "checked" : "" }}>3<br>
        <input type="radio" name="rating" value="4" {{ ($data['rating'])==4 ? "checked" : "" }}>4<br>
        <input type="radio" name="rating" value="5" {{ ($data['rating'])==5 ? "checked" : "" }}>5<br>    
    </td>
    <br>
    <div style="color:red">{{ $errors->first('rating')}}</div>
    <button type='submit' class="button" style="vertical-align:middle">儲存變更</button>
</form>

<a href={{'/admin/comment/'.$country}}>
    <button  class="btn btn-danger" style="vertical-align:middle"><span>取消</span></button>  
</a>
</x-app-layout>



<script>
$(document).ready(function(){

  $('#textarea').bind('input propertychange', function() {
      if (this.value.length > 50) {
        $("#alert").show();
        $("#textarea").val($("#textarea").val().substring(0,50));
      }
      else{
        $("#alert").hide();
      }
    });

});


</script>