<!-- 顯示所有User -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

.table {
  table-layout: fixed; 
  width: 1000px; 
  padding: 5px;
}
th{
  text-align:center;
}
tr{
  text-align:center;
}

</style>
<x-app-layout>

<div class="table-responsive-md container-sm p-3">
  <table class="table table-dark table-hover table-responsive-md ">
      <tr>
          <th style="width:90px">使用者ID</th>
          <th style="width:90px">名字</th>
          <th style="width:300px">電子信箱</th>
          <th style="width:90px">使用者型態</th>
          <th>手機</th>
          <th>功能</th>
      </tr>
      @foreach($users as $user)
      <tr>
          <td>{{$user['id']}}</td>
          <td>{{$user['name']}}</td>
          <td>{{$user['email']}}</td>
          <td>{{($user['usertype']==1)? "管理員" : "會員"}}</td>
          <td>{{$user['phone']}}</td>
          <td>
            <a class='btn btn-danger' href={{"userDelete/".$user['id']}}  onClick="return confirm('確定要刪除嗎？');">刪除</a>
            <a class='btn btn-warning' href={{"userEdit/".$user['id']}}>編輯</a>
          </td>    
      </tr>    
      @endforeach
  </table>
</div>
<div class="container-sm p-3">
  <a  href = '/redirect'>
      <button onclick="goto()" class="button " style="vertical-align:middle" ><span>回首頁</span></button>  
  </a>
</div>
</x-app-layout>
