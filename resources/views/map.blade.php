<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SJHotel日新飯店地圖</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,500' rel='stylesheet' type='text/css'>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDSN747FpZTAGiN4nbTbWCJQl0HeduVUTw" type="text/javascript"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
.container {
  border: 10px solid #1d3557;
  padding: 10px;
  
}
.item {
  width:800px;
  padding: 5px; 
}
.container-sm span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.container-sm:hover span {
  padding-bottom: 15px;
  padding-right: 15px;
}

</style>

<x-app-layout>
<html> 
<!--<center>-->
<head> 
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
    <title>Google Maps Multiple Markers</title> 
    
</head>

<div class="container-sm p-3" id="header-wrapper"></div>

<body>
     
    @if (count($hotels) == 0) <!-- 資料庫查不到 -->
    <div style="font-size:50px;">{{$error_message}}</div>
    <form action="{{url('/redirect')}}" method="get">
        <button type="submit" class="button container-sm p-3" style="vertical-align:middle"><span>重新查詢 </span></button>        
    </form>
    @else <!-- 有找到 -->
    <div class="container" style="width: 1000px;">
      <div id="map" class="container-md p-3 " style="width: 750px; height: 400px;"></div>

      <div class="container-md p-3 "></div>
      <div class="item container-md p-3" id="side_bar" style="overflow:auto; height:450px"></div>
    </div>
    <form action="{{url('/redirect')}}" method="get">
        <button type="submit" class="button" style="margin-left: 600px;margin-right: 600px;"><span>重新查詢 </span></button>        
    </form>
    @endif

    <script type="text/javascript">
        var latlng = [];
        
        @foreach ($hotels as $hotel)
        latlng.push([{{$hotel->id}},'{{$hotel->hotelname}}',{{$hotel->latitude}},   //0,1,2
                    {{$hotel->longitude}},'{{$hotel->address}}','{{$hotel->url}}',  //3,4,5
                    {{$hotel->avg_rating}},{{$hotel->total_comments}}])             //6,7
        @endforeach
      
        var side_bar_html = "";  
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: new google.maps.LatLng(latlng[0][2],latlng[0][3]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var infowindow = new google.maps.InfoWindow();
        var markers = []
        var marker, i;
        
        for (i = 0; i < latlng.length; i++) {  
            marker = new google.maps.Marker({
            position: new google.maps.LatLng(latlng[i][2], latlng[i][3]),
            map: map
            });
        
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
            infowindow.setContent(latlng[i][1]);
            infowindow.setContent('<span><div class="container-sm p-3 "><div style="background-color:#ee9b00"><strong>' + latlng[i][1] +
                          '</strong><br>' + '地址 : ' + latlng[i][4] + '</div>'+
                          '<div> 評分 : '+latlng[i][6]+'</div><div> 共'+latlng[i][7]+'則評論<div>'+
                          '<div><strong>飯店網站連結 : </strong><br>'+
                          '<a href='+latlng[i][5]+'>'+latlng[i][1]+'</a></div></span>');
            infowindow.open(map, marker);
            }
        })(marker, i));
        markers.push(marker)
        
 
        }
        
        var i = 0
        @foreach ($hotels as $hotel)
        side_bar_html +='<span><div id="info" class="container-md p-3 border-4 border-dark border-top-0 border-bottom-0 border-right-0 " style="box-shadow: 4px 4px 8px 0 grey;background-color:">'+
                        '<a href="javascript: myclick('+ (i) +')">' + 
                        '<p>飯店名稱 : '+'{{$hotel->hotelname}}'+'</p><p>飯店地址 : '+'{{$hotel->address}}'+
                        '</p><a href='+'{{$hotel->url}}'+'>點我查看飯店連結</a>' + 
                        '<div ><a href = {{ url('search/result/'.$country.'/'.$hotel->hotel_id.'/'.$location.'/comment') }}>點我查看會員評論</a></div>'+ 
                        '</a></div><div class="container-sm p-3"></div></span>';
        
        i+=1
        
        @endforeach             // url('search/'.$country.'/'.$site->id.'/message')
        document.getElementById("side_bar").innerHTML = side_bar_html


        function myclick(i) {
            // zoom to marker so added to map
            map.setCenter(markers[i].getPosition());
            map.setZoom(19);
            google.maps.event.trigger(markers[i], "click");
        }
    
        
    </script>
    <!--</center>-->
</body>
</html>

</x-app-layout>