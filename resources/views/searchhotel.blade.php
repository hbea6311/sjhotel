<x-app-layout>
<html>
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color:#000000;
  border: none;
  color: #ffffff;
  text-align: center;
  font-size: 20px;
  padding: 20px;


  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}



.button:hover span:after {
  opacity: 1;
  right: 0;
}
</style>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title></title>
</head>

<body>

<div id="header-wrapper">
    <center><div style="font-size:50px;">請先選擇查詢國家並輸入地區名稱</div>
    <div id="header" class="container ">
        <div id="logo" >
            <div style="font-size:30px;"<a href="#">SJ HOTEL日本新加坡飯店地圖</a></div>
            </center>
    </div>
    <br>


<center>
<table  style="width: 40%;">
<tr>
<td><img src="{{URL('images/jp.jpeg')}}" alt=""> </td>
<td><img src="{{URL('images/sg.jpeg')}}" alt=""></td>
</tr>
</table>

<br>
<td>
<form action="{{ url('search/result') }}" method="get" >
<input type="radio" style="border-radius:20px" name="country" value="jp" checked>日本
<input type="radio" style="border-radius:20px" name="country" value="sg">新加坡
<br>

<form action="{{url('/redirect')}}" method="get">
<input  style="border:4px black solid;" type="text" class="form-controller" placeholder="&nbsp&nbsp請輸入地區名稱"  id="search" name="search" required></input>

<button type="submit" class="button" style="vertical-align:middle"><span>查詢 </span></button>

    </form>
</td>
</tr>

</table>
</center>

</div>



</div>

</body>

</html>
</x-app-layout>

