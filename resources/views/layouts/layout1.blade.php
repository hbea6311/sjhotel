<html>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport'contect='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content = 'ie=edge'>
    <link rel='stylesheet' href = '{{asset('css/app.css')}}'>
    <title>document</title>
</head>
<body class='bg-gradient-to-r from-gray-100 to-gray-200'>
    @yield('content')
</html>