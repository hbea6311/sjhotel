<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSghotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sghotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hotel_id');
            $table->string('hotelname');
            $table->string('address');
            $table->string('url');
            $table->decimal('longitude', 10, 5);
            $table->decimal('latitude', 10, 5);
            $table->decimal('avg_rating', 2, 1)->nullable();
            $table->bigInteger('total_comments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sghotels');
    }
}
