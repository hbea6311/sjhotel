<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJpcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jpcomments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->string('hotel_id');
            $table->string('content', 500)->nullable();
            $table->bigInteger('rating');
            $table->timestamps();
            #$table->foreign('user_id')->references('id')->on('users');
            #$table->foreign('site_id')->references('hotel_id')->on('jphotel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jpcomments');
    }
}
