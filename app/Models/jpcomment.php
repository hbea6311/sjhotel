<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jpcomment extends Model
{
    use HasFactory;
    protected $table = 'jpcomments';
    protected $fillable = [
        'user_id', 'hotel_id', 'content', 'rating'
    ];
}
