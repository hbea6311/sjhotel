<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jphotel extends Model
{
    use HasFactory;
    protected $table = 'jphotels';
    protected $primaryKey = 'id';

    public static function getHotels() {

    	// return TaiwanSite::select('hotelname', 'address','phonenumber','longtitude','latitude')->get();
        return JapanHotel::select('hotelname', 'address','phonenumber','longtitude','latitude')->get();
    }
}
