<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sgcomment extends Model
{
    use HasFactory;
    protected $table = 'sgcomments';
    protected $fillable = [
        'user_id', 'hotel_id', 'content', 'rating'
    ];
}
