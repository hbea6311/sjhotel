<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sghotel extends Model
{
    use HasFactory;
    protected $table = 'sghotels';
    protected $primaryKey = 'id';

    public static function getHotels() {

    	return SingaporeHotel::select('hotelname', 'address','longtitude','latitude')->get();
    }
}
