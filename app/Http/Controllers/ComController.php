<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Auth;
use App\Models\Jphotel;
use App\Models\jpcomment;
use App\Models\Sghotel;
use App\Models\Sgcomment;
use App\Models\User;

class ComController extends Controller
{
    //查看特定一間飯店所有評論
    public function hotel_comment($country,$hotel_id,$location)
    {
        
        if (Auth::check()) {
            $user = Auth::user();
            $name_arr=[];
            if($country =='jp'){

                $hotel = Jphotel::where('hotel_id',$hotel_id)->get();
                $comment = jpcomment::where('hotel_id', $hotel_id)
                ->orderBy('created_at', 'desc')->get();
                
                //error_log($comment);
                for($i=0;$i<count($comment);$i++){
                    
                    $uid = $comment[$i]->user_id;
                    $uname = User::where('id',$uid)->select('name')->get();
                    
                    
                    array_push($name_arr,$uname[0]->name);
                   
                }
                
                
                return view('comment', compact('user','hotel','country','comment','location','name_arr'));  
            }
            elseif($country =='sg'){
                $hotel = Sghotel::where('hotel_id',$hotel_id)->get();
                $comment = Sgcomment::where('hotel_id', $hotel_id)
                ->orderBy('created_at', 'desc')->get();
                for($i=0;$i<count($comment);$i++){
                    error_log($comment[$i]->user_id);
                    $uid = $comment[$i]->user_id;
                    $uname = User::where('id',$uid)->select('name')->get();
                    array_push($name_arr,$uname[0]->name);
                
                }
                return view('comment', compact('user','hotel','country','comment','location','name_arr'));  
            }
           
            #return view('comment', compact('user','hotel','country','comment','location'));      
        }  
    }
    //查看使用者所有評論
    public function user_comment($country = 'jp'){
        if (Auth::check()) {
            $user = Auth::user();
            $userid = $user->id;
            $usertype = $user->usertype;
            if ($usertype == 1){    //管理員
                if($country == 'jp'){
                    $data = jpcomment::all();
                    return view('/admin/manage_com',compact('country'),['comments'=>$data]);
                }
                else if($country == 'sg'){
                    $data = Sgcomment::all();
                    return view('/admin/manage_com',compact('country'),['comments'=>$data]);
                }
            }
            else{                   //一般使用者
                if($country == 'jp'){
                    $hotelnames = [];
                    $comments = jpcomment::where('user_id', $userid)
                            ->orderBy('created_at','desc')->get();

                            for($i = 0;$i<count($comments);$i++)
                            {
                                $hotel_id = $comments[$i]->hotel_id;

                                $hotelname = Jphotel::where('hotel_id',$hotel_id)->select('hotelname')->get();
                                array_push($hotelnames,$hotelname[0]->hotelname);

                            }
                    error_log($userid);            
                    return view('show_user_comment',compact('userid','country','comments','hotelnames'));
                }
                elseif($country=='sg'){
                    $hotelnames = [];
                    $comments = Sgcomment::where('user_id', $userid)
                            ->orderBy('created_at','desc')->get();

                            for($i = 0;$i<count($comments);$i++)
                            {
                                $hotel_id = $comments[$i]->hotel_id;

                                $hotelname = Sghotel::where('hotel_id',$hotel_id)->select('hotelname')->get();
                                array_push($hotelnames,$hotelname[0]->hotelname);

                            }

                    return view('show_user_comment',compact('userid','country','comments','hotelnames'));
                }
            }
            
        }

    }
    // 新增評論
    public function store(Request $request, $country, $hotel_id){
 
        if (Auth::check()) {
            $uid = Auth::id();
            $validator = Validator::make($request->all(),[
                'content' => 'nullable|string|max:50',
                'rating' => 'required|integer',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
            else{
                if($country == 'jp'){
                    $comment = $request->content;
                    $rating = $request->rating;
                    $comment = jpcomment::create([
                        'user_id' => $uid,
                        'hotel_id' => $hotel_id,
                        'content' => $request->content,
                        'rating' => $request->rating,
                    ]);
                    $comment->save();
                    #return view('temp',compact('uid','country','hotel_id','comment','rating'));
                    $this->updateRating($request, $country, $hotel_id);
                    return redirect()->back();
                }
                elseif($country=='sg'){
                    $comment = $request->content;
                    $rating = $request->rating;
                    $comment = sgcomment::create([
                        'user_id' => $uid,
                        'hotel_id' => $hotel_id,
                        'content' => $request->content,
                        'rating' => $request->rating,
                    ]);
                    $comment->save();
                    #return view('temp',compact('uid','country','hotel_id','comment','rating'));
                    $this->updateRating($request, $country, $hotel_id);
                    return redirect()->back();
                    
                    return redirect()->back();
                }
            }
            
        }
    }
    //刪除評論
    public function delete_comment(Request $request,$country, $id){
        if ($country == 'jp') {
            $comment = jpcomment::findOrFail($id);        
        }
        elseif($country =='sg'){
            $comment = Sgcomment::findOrFail($id);
        }
        $comment->delete();
        $hotel_id = $comment->hotel_id;
        $this->updateRating($request, $country, $hotel_id); 
        
        return redirect()->back();
        
    }
    //編輯評論
    public function update_comment(Request $request, $id, $country) {
        $validator = Validator::make($request->all(), [
            'content' => 'nullable|string|max:50',
            'rating' => 'nullable|integer',
        ]);
        
        if ($validator->fails()) {
            #error_log($validator->errors());
            return redirect()->back()->withErrors($validator);
        }
        else{
            if ($country == 'jp') {
                $comments = jpcomment::find($id);
                $comments->content = $request->content;
                $comments->rating = $request->rating;
                $comments->save();
            }
            else{
                $comments = Sgcomment::find($id);
                $comments->content = $request->content;
                $comments->rating = $request->rating;
                $comments->save();
            }
            // 更新評論的平均評分和評分個數
            $hotel_id = $comments->hotel_id;
            $this->updateRating($request, $country, $hotel_id); 
            #return redirect('/usercomment');
            return redirect()->back();
        }
    }

    public function updateRating(Request $request, $country, $hotel_id){
        if ($country == 'jp') {
            $hotel_table = 'jphotels';
            $messages_table = 'jpcomments';
        }
        elseif($country =='sg'){
            $hotel_table = 'sghotels';
            $messages_table = 'sgcomments';
        }
        $new_avg_rating = DB::table($messages_table)
                ->where('hotel_id', $hotel_id)
                ->avg('rating');
        $new_total_comments = DB::table($messages_table)
            ->where('hotel_id', $hotel_id)
            ->count('rating');
        
        $new_hotel = DB::table($hotel_table)
            ->where('hotel_id', $hotel_id)
            ->update(['avg_rating' => $new_avg_rating, 'total_comments' => $new_total_comments]); 
    }
    /*  合併到user_comment去了
    public function show($country = 'jp'){
        if($country == 'jp'){
            $data = jpcomment::all();
            return view('/admin/manage_com',compact('country'),['comments'=>$data]);
        }
        else if($country == 'sg'){
            $data = Sgcomment::all();
            return view('/admin/manage_com',compact('country'),['comments'=>$data]);
        }
        
    }*/
    public function delete($country, $id){
        if($country == 'jp'){
            $data = jpcomment::find($id);
            $data -> delete();
            return redirect('/admin/comment/jp');  
        }
        else if($country == 'sg'){
            $data = Sgcomment::find($id);
            $data -> delete();
            return redirect('/admin/comment/sg');  
        }       
    }

    public function showData($country,$id){
        if($country == 'jp'){
            $data= jpcomment::find($id);
            return view('admin/manage_com_edit',compact('country','id'),['data'=>$data]);
        }
        if($country == 'sg'){
            $data= Sgcomment::find($id);
            return view('admin/manage_com_edit',compact('country','id'),['data'=>$data]);
        }    
    }

    public function update($country,$id,Request $req){
        
        $validator = Validator::make($req->all(), [
            'content'=>'nullable|string|max:50',
            'rating'=>'required',
        ]);

        if ($validator->fails()) {
            #error_log($validator->errors());
            // return redirect()->back()->withErrors($validator);
            return redirect()->back()->withErrors($validator);
        }

        else{
            if($country == 'jp'){
                $data=jpcomment::find($req->id);
                $data->content=$req->content;
                $data->rating=$req->rating;
                $data->save();
                return redirect('admin/comment/jp');    
            }
            
            if($country == 'sg'){
                $data=Sgcomment::find($req->id);
                $data->content=$req->content;
                $data->rating=$req->rating;
                $data->save();
                return redirect('admin/comment/sg');    
            }
        }        
    }
}
