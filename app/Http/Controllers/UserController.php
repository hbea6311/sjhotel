<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\jpcomment;
use App\Models\Jphotel;
use App\Models\Sgcomment;
use App\Models\Sghotel;

class UserController extends Controller
{
    public function show(){
        $data = User::all();
        return view('/admin/manage_member',['users'=>$data]);
    }
    public function delete($id){
        //刪除jp評論
        $jpcomment_data = jpcomment::where('user_id',$id);
        jpcomment::where('user_id',$id)-> delete(); 
        //刪除sg評論
        $Sgcomment_data = Sgcomment::where('user_id',$id);
        Sgcomment::where('user_id',$id)-> delete();
        //刪除使用者
        $data = User::find($id);
        $data -> delete();
        
        return redirect('/admin/manage_member');
    }

    public function showData($id){
        $data= User::find($id);
        return view('admin/userEdit',['data'=>$data]);
    }

    public function update(Request $req){

        $validator = Validator::make($req->all(), [
            'name'=>'required|string|max:20',
            // 'email' => 'required|string|email|max:255|Rule::unique('users')->ignore($req->id),
            // 'email' => 'required|string|email|max:255|unique:users,id,'.$req->id,
            'email' => ['required','string','email','max:255',Rule::unique('users')->ignore($req->id),],
            'usertype'=>'required|string',
            'phone' => 'required|string|digits:10',
        ]);
        
        if ($validator->fails()) {
            #error_log($validator->errors());
            // return redirect()->back()->withErrors($validator);
            return redirect()->back()->withErrors($validator);
        }

        else{
            $data=User::find($req->id);
            $data->name=$req->name;
            $data->email=$req->email;
            $data->usertype=$req->usertype;
            $data->phone=$req->phone;
            // $data->password=$req->password;
            $data->save();
            return redirect('/admin/manage_member'); 
        }
       
    }


}
