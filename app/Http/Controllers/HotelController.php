<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Auth;
use App\Models\Jphotel;
use App\Models\Sghotel;
use App\Models\jpcomment;
use App\Models\Sgcomment;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dashboard');
    }    

    public function search(Request $request)
    {
    	$validator = Validator::make($request->all(),[
            'country' => ['required', 'string'],
            'search' => ['required', 'string'],
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator);

        } 
        else{
            if($request->country == 'jp'){
                $country = $request->country;
                $location = $request->search;
                $search_address = Jphotel::where('address', 'like', '%'.$request->search.'%')->get();
                $search_name = Jphotel::where('hotelname', 'like', '%'.$request->search.'%')->get();
                $hotels = $search_address->union($search_name);
                $hotels = $hotels->take(10);
            }
            elseif($request->country == 'sg'){
                $country = $request->country;
                $location = $request->search;
                $search_address = Sghotel::where('address', 'like', '%'.$request->search.'%')->get();
                $search_name = Sghotel::where('hotelname', 'like', '%'.$request->search.'%')->get();
                error_log($search_address);
                error_log($search_name);
                $hotels = $search_address->union($search_name);
                $hotels = $hotels->take(10);
            }
            else{
                return redirect()->back();
            }
            
            if ($hotels->isEmpty()) {

                $error_message = "查無相似飯店";
                return view('map', compact('hotels','country','error_message'));
            }
            else{
                return view('map', compact('hotels','country','location'));
            }
            
        }
    }

    public function seecomment($country,$hotel_id,$location)
    {
        
        if (Auth::check()) {
            $user = Auth::user();
            
            if($country =='jp'){
                $hotel = Jphotel::where('hotel_id',$hotel_id)->get();
                $comment = jpcomment::where('hotel_id', $hotel_id)
                ->orderBy('created_at', 'desc')->get();
                return view('comment', compact('user','hotel','country','comment','location'));  
            }
            elseif($country =='sg'){
                $hotel = Sghotel::where('hotel_id',$hotel_id)->get();
                $comment = Sgcomment::where('hotel_id', $hotel_id)
                ->orderBy('created_at', 'desc')->get();
                return view('comment', compact('user','hotel','country','comment','location'));  
            }
           
            #return view('comment', compact('user','hotel','country','comment','location'));      
        }  
    }

    /*
    //這不會用到
    public function seeallcomment($country = 'jp'){
        if($country == 'jp'){
            $comments = jpcomment::latest()
                            ->paginate(10);
            return view('admin.manage_com',compact('country','comments'));
        }
        else{
            return view('admin.manage_com',compact('country'));
        }
        
    }
    //這也不會用到
    public function searchcomment(Request $request,$country){
        $validator = Validator::make($request->all(),[
            'search' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        else{
            if($country == 'jp'){
                $hotel_id = $request->search;
                $comments = jpcomment::where('hotel_id', $hotel_id)
                    ->orderBy('created_at', 'desc')->get();
                return view('admin.manage_com',compact('country','comments'));
            }
        }
        
    }*/
}
