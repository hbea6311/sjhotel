<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\ComController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    #return view('welcome');
    return view('auth.login');
});

#Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/usercomment', [ComController::class,'user_comment'])->name('usercomment');
Route::get('/usercomment/{country}',[ComController::class,'user_comment']);
Route::delete('/usercomment/{country}/{id}/delete',[Comcontroller::class,'delete_comment']);
Route::post('/usercomment/{id}/{country}/edit',[Comcontroller::class,'update_comment']);

Route::get('/redirect',[HomeController::class,'redirect']);

Route::get('/search/result',[HotelController::class,'search']);

Route::get('/search/result/{country}/{hotel_id}/{location}/comment',[ComController::class,'hotel_comment']);
Route::post('/search/result/{country}/{hotel_id}/comment', [ComController::class,'store']);
//Route::get('/search/result/{country}/{hotel_id}/comment', [ComController::class,]);

// Route::get('/admin/manage_comment',[HotelController::class,'seeallcomment']);
// Route::get('/admin/manage_comment/{country}',[HotelController::class,'seeallcomment']);
// Route::get('/admin/manage_comment/{country}/search',[HotelController::class,'searchcomment']);

//管理員管理評論
#Route::get('/admin/comment/{country}',[ComController::class,'show']);//印出所有評論
Route::get('/admin/comment/{country}',[ComController::class,'user_comment']);//印出所有評論
Route::get('/admin/commentDelete/{country}/{id}',[ComController::class,'delete']);//刪除評論
Route::get('/admin/commentEdit/{country}/{id}',[ComController::class,'showData']);//印出要編輯的評論
Route::post('/admin/commentEdit/{country}/{id}',[ComController::class,'update']);//編輯評論

//管理員管理會員
Route::get('/admin/manage_member',[UserController::class,'show']);//印出所有會員
Route::get('/admin/userDelete/{id}',[UserController::class,'delete']);//刪除會員
Route::get('/admin/userEdit/{id}',[UserController::class,'showData']);//印出要編輯的會員
Route::post('/admin/userEdit',[UserController::class,'update']);//編輯會員
